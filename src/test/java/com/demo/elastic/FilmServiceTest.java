package com.demo.elastic;


import com.demo.elastic.interf.FilmRepository;
import com.demo.elastic.model.Film;
import com.demo.elastic.service.interf.FilmService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest(classes = ElasticApplication.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class FilmServiceTest {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private FilmService filmService;

    @Autowired
    private static ElasticsearchRestTemplate esTemplate;


    @Test
    public void testSave() {

        Film film = new Film("1001", "Le ciel et la mer");
        Film testFilm = filmRepository.save(film);

        Assertions.assertNotNull(testFilm.getId());
        Assertions.assertEquals(testFilm.getTitle(), film.getTitle());

    }

    @Test
    public void testFindOne() {

        Film film = new Film("1001", "Le ciel et la mer");
        filmRepository.save(film);

        Optional<Film> optionalFilm = filmRepository.findById(film.getId());

        Assertions.assertNotNull(optionalFilm);
        Assertions.assertEquals(optionalFilm.get().getTitle(), film.getTitle());

    }

    @Test
    public void testFindByTitle() {

        Film film = new Film("1001", "Le ciel et la mer");
        filmRepository.save(film);

        List<Film> byTitle = filmService.findByTitle(film.getTitle());
        assertThat(byTitle.size(), is(1));
    }

    @Test
    public void testFindByActor() {

        List<Film> filmList = new ArrayList<>();
        Film film1 = new Film("1001", "Le ciel et la mer");
        film1.setActor("Rene Edouard");
        Film film2 = new Film("1002", "La lune et le soleil");
        film2.setActor("Rene Edouard");

        filmRepository.saveAll(Arrays.asList(film1, film2));

        Page<Film> byActor = filmService.findByActor("Rene Edouard", PageRequest.of(0, 10));
        assertThat(byActor.getTotalElements(), is(2L));

    }

    @Test
    public void testDelete() {

        Film film = new Film("1001", "Le ciel et la mer");
        filmRepository.save(film);
        filmRepository.delete(film);

        Assertions.assertNull(filmRepository.findById(film.getId()));
    }

}
