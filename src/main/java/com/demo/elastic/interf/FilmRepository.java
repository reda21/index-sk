package com.demo.elastic.interf;

import com.demo.elastic.model.Film;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface FilmRepository extends ElasticsearchRepository<Film, String> {

        Page<Film> findByActor(String actor, Pageable pageable);

        List<Film> findByTitle(String title);

}
