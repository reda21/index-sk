package com.demo.elastic;

import com.demo.elastic.interf.FilmRepository;
import com.demo.elastic.model.Film;
import com.demo.elastic.service.interf.FilmService;
import org.elasticsearch.client.Node;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@SpringBootApplication
@RestController
public class ElasticApplication extends SpringBootServletInitializer implements CommandLineRunner {

	@Autowired
	private ElasticsearchOperations es;

	@Autowired
	private FilmService filmService;

	@Autowired
	private FilmRepository filmRepository;

	@Autowired
	RestHighLevelClient highLevelClient;


	public static void main(String[] args) {
		SpringApplication.run(ElasticApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		Film film1 =	new Film("1001", "Ciel 1");
		film1.setActor("Rene");
		Film film2 =	new Film("1002", "Ciel 2");
		film2.setActor("Rene");
		Film film3 =	new Film("1003", "Ciel 3");
		film3.setActor("Rene");

		filmRepository.saveAll(Arrays.asList(film1, film2, film3));

		printElasticSearchInfo();
	}

	private void printElasticSearchInfo() throws Exception {

		Page<Film> films = filmService.findByActor("Rene", PageRequest.of(0, 10));

		films.forEach(x -> System.out.println(x.getTitle()));

		Optional<Film> optionalFilm = filmRepository.findById("1001");

		System.out.println(optionalFilm.isPresent()? optionalFilm.get().getTitle(): "aucun resultat");

	}

}
