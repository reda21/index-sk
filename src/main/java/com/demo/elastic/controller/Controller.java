package com.demo.elastic.controller;

import com.demo.elastic.model.Film;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.*;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/films")
public class Controller implements ErrorController {

    private  ElasticsearchOperations elasticsearchOperations;

    public Controller(ElasticsearchOperations elasticsearchOperations) {
        this.elasticsearchOperations = elasticsearchOperations;
    }

    @PostMapping("/film")
    public String save(@RequestBody Film film) {

        IndexQuery indexQuery = new IndexQueryBuilder()
                .withId(film.getId().toString())
                .withObject(film)
                .build();

        String documentId = elasticsearchOperations.index(indexQuery, null);
        return documentId;
    }

    @GetMapping("/film/{id}")
    public Film findById(@PathVariable("id")  String id) {
        QueryBuilder matchQuery = QueryBuilders.termQuery("id", id);
        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(matchQuery)
                .build();
        SearchHits<Film> film = elasticsearchOperations
                .search(searchQuery, Film.class);

        return film.get().findFirst().get().getContent();
    }

    @GetMapping("film/{actor}")
    public List<Film> findByActor(@PathVariable("actor") String pActor) {

        List<Film> filmReturned = new ArrayList<>();

        QueryBuilder matchQuery = QueryBuilders.termQuery("actor", pActor);
        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(matchQuery)
                .build();
        SearchHits<Film> films = elasticsearchOperations
                .search(searchQuery, Film.class);

        films.stream().forEach(film -> filmReturned.add(film.getContent()));

        return filmReturned;
    }

    @Override
    @RequestMapping("error")
    @ResponseBody
    public String getErrorPath() {

        return "No Mapping Found";
    }
}
