package com.demo.elastic.service.impl;

import com.demo.elastic.interf.FilmRepository;
import com.demo.elastic.model.Film;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import com.demo.elastic.service.interf.FilmService;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Configuration
@Service("filmService")
@EnableElasticsearchRepositories(basePackages = "com.demo.elastic.interf")
@ComponentScan(basePackages = {"com.custom.sakila.service"})
@EntityScan("com.demo.elastic.model")
public class FilmServiceImpl implements FilmService {

    private FilmRepository filmRepository;

    @Autowired
    public void setFilmRepository(FilmRepository filmRepository) {
        this.filmRepository = filmRepository;
    }

    @Override
    public Page<Film> findByActor(String actor, PageRequest pageRequest) {
        return filmRepository.findByActor(actor, pageRequest);
    }
    @Override
    public List<Film> findByTitle(String title) {
        return filmRepository.findByTitle(title);
    }

}
