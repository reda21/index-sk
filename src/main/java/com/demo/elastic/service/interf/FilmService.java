package com.demo.elastic.service.interf;

import com.demo.elastic.model.Film;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface FilmService {

    Page<Film> findByActor(String actor, PageRequest pageRequest);

    List<Film> findByTitle(String title);

}
